# README #

### ESP32 BLE MQTT Lumin+ CheerLight Project ###

I was recently told about the [Cheerlights](https://cheerlights.com) project and thought it would be a fun way to leverage the previous knowledge presented in the [BLE Sniffing videos](https://www.youtube.com/watch?v=JIh2YYwkzoE&list=PLQi2cySxF1TxDKTIiAx5iVUCH4RRMDgfp).

So, using the Lumin+ from [Five and Below](https://www.fivebelow.com/products/lumin-bluetooth-smart-light-w-app-control) we will write some code using the Arduino IDE for the ESP32 to connect to our home WiFi network, subscribe to the Cheerlights MQTT channel, and using BLE, we will update the Lumin+ RGB color each time a user sends the proper message to Cheerlights on Twitter.

The project promises to show you a lot of code network code as well as answer a few questions I have received since the BLE Sniffing project was posted.

Hang tight, boot your computer and grab an EPS32 development board!  The fun is just a click away.
   






