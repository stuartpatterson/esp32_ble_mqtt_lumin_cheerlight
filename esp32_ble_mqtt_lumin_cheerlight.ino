//==================================================================================================
//
// esp32_ble_mqtt_lumin_cheerlight.ino
//    see readme.md for details and to watch the Youtube video related to this source code.
//
//  you will need to setup your Arduino IDE to build esp32 firmware - search online
//
//  you will also need - 
//  PubSubClient for MQTT from https://github.com/knolleary/pubsubclient/
//  AutoConnect - https://github.com/Hieromon/AutoConnect
//
//  you must set the partition site to Minimal SPIFFS (1.9MB App...) under Arduino IDE
//  Tools/Partition Scheme menu
//==================================================================================================

#include <WiFi.h>
#include <WebServer.h>
#include <AutoConnect.h>
#include <PubSubClient.h>
#include <BLEDevice.h>
#include <string.h>

const char *mqttServer = "mqtt.cheerlights.com";
unsigned int mqttPort = 1883;

const char *BLE_serverName = "LD-0003"; // I have also seen the lumin+ with the BLE server name of "Triones-A40100012F01"
static BLEUUID serviceUUID("ffd5");     // The remote service we wish to connect to.
static BLEUUID  charUUID("ffd9");       // The characteristic of the remote service we are interested in.

uint8_t rgb_color_data[] = { 0x56, 0x00, 0x00, 0x00, 0x01, 0xf0, 0xaa };
uint8_t off_command[] = { 0xcc, 0x24, 0x33 }; //Off service=ffd5, characteristic=ffd9, value=cc2433 
uint8_t on_command[] = { 0xcc, 0x23, 0x33 };  //On service=ffd5, characteristic=ffd9, value=cc2333 

static boolean doBLEConnect = false;
static boolean BLEconnected = false;
static boolean doBLEScan = false;
static boolean doBLERGB = false;
static BLERemoteCharacteristic* pRemoteCharacteristic;
static BLEAdvertisedDevice* myDevice;

WebServer webServer;
AutoConnect Portal(webServer);
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void rootPage()
{
  char content[] = "<meta http-equiv=\"refresh\" content=\"0; url=_ac\">";
  webServer.send(200,"text/html",content);
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void setupMQTT()
{
  mqttClient.setServer(mqttServer, mqttPort); // connect to mqtt server
  mqttClient.setCallback(MQTTcallback);       // and register a callback function
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void MQTTreconnect()
{
  while (!mqttClient.connected()) {
    String clientId = "ESP32Lumin_Client-";
    clientId += String(random(0xffff), HEX);
    
    if (mqttClient.connect(clientId.c_str())) {
      mqttClient.subscribe("cheerlightsRGB"); // subscribe to topic
    }
  }
}

//--------------------------------------------------------------------------------------------------
// MQTTcallback will be called each time data is available to consume that I have 
//  subscribed to on the MQTT server
//  the message should be the string like "#FFFFFF" where each by (FF) is a color in the order of RGB
//--------------------------------------------------------------------------------------------------
void MQTTcallback(char* topic, byte* payload, unsigned int length) {
 
  if ( length == 7 && payload[0] == '#' ) {
    char byte_code[3];
    byte_code[2] = '\0';   // add null at end of string
    int offset = 1;
    for( int i=1; i < 4; ++i ) {
      strncpy(byte_code, (char *)(payload+offset), 2);   // 1, 3, 5
      byte byte_val = (byte)strtol(byte_code, NULL, 16);
      rgb_color_data[i] = byte_val;
      offset += 2;
    }
    
    //uint8_t rgb_color_data[] = { 0x56, 0x00, 0x00, 0x00, 0x01, 0xf0, 0xaa };
    doBLERGB = true;
  }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
class MyClientCallback : public BLEClientCallbacks
{
  //--------------------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------------------
  void onConnect(BLEClient* pclient)
  {  }

  //--------------------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------------------
  void onDisconnect(BLEClient* pclient)
  {
    BLEconnected = false;
  }
};

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void setupWIFI()
{
  AutoConnectConfig config;
  
  config.apid = "esp32_cheerlight";
  config.psk = "";
  Portal.config(config);
  
  webServer.on("/",rootPage);
  Portal.begin();
  Serial.println("Web Server started:" + WiFi.localIP().toString());
}

//--------------------------------------------------------------------------------------------------
// Scan for BLE servers and find the first one that advertises the service we are looking for.
//--------------------------------------------------------------------------------------------------
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks
{
  //--------------------------------------------------------------------------------------------------
  // Called for each advertising BLE server.
  //--------------------------------------------------------------------------------------------------
  void onResult(BLEAdvertisedDevice advertisedDevice) {

    //Name: LD-0003, Address: ac:02:00:02:03:ed
    if ( advertisedDevice.haveName() ) {
      const char *service_name = advertisedDevice.getName().c_str();
      if ( strcmp(service_name, BLE_serverName) == 0 ) {
        BLEDevice::getScan()->stop();
        myDevice = new BLEAdvertisedDevice(advertisedDevice);
        doBLEConnect = true;
        doBLEScan = true;
        Serial.println("BLE device found during scan!");
      }
    } 
  } 
};

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void setupBLE()
{
  BLEDevice::init("");

  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 5 seconds.
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(5, false);
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
bool connectToBLEServer()
{
    BLEClient*  pClient  = BLEDevice::createClient();
    pClient->setClientCallbacks(new MyClientCallback());

    // Connect to the remove BLE Server.
    pClient->connect(myDevice);  // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
    pClient->setMTU(517); //set client to request maximum MTU from server (default is 23 otherwise)
  
    // Obtain a reference to the service we are after in the remote BLE server.
    BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
    if (pRemoteService == nullptr) {
      pClient->disconnect();
      return false;
    }

    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
    if (pRemoteCharacteristic == nullptr) {
      pClient->disconnect();
      return false;
    }

    // Read the value of the characteristic.
    if(pRemoteCharacteristic->canRead()) {
      std::string value = pRemoteCharacteristic->readValue();
    }

    Serial.println("BLE device connected!");
    BLEconnected = true;
    return true;
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void setup()
{
  Serial.begin(115200);

  setupWIFI();    // need for MQTT communications
  setupMQTT();
  setupBLE();
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
void loop() {
  static uint8_t r=0,g=0,b=0;

  if (WiFi.status() == WL_CONNECTED)
  { 
    if (!mqttClient.connected())
      MQTTreconnect();
  
      mqttClient.loop();  

    // If the flag "doConnect" is true then we have scanned for and found the desired
    // BLE Server with which we wish to connect.  Now we connect to it. 
    if (doBLEConnect == true) {
      if (connectToBLEServer()) {
        doBLEConnect = false;
      } 
    }

    // If we are connected to a peer BLE Server update the RGB if it has changed
    if ( BLEconnected ) {
      if ( pRemoteCharacteristic != NULL ) {
        if ( pRemoteCharacteristic->canWriteNoResponse() && doBLERGB ) {

          for(int x=0; x < sizeof(rgb_color_data); ++x) {
            byte i;
            i = rgb_color_data[x];
            Serial.printf("%.2X ",i);
          }

          Serial.println("\n Sending RGB data to lumin device");
          pRemoteCharacteristic->writeValue(rgb_color_data, sizeof(rgb_color_data));
          doBLERGB = false;
        }
      }
    }
    else if(doBLEScan){
      BLEDevice::getScan()->start(0);  // this is just example to start scan after disconnect, most likely there is better way to do it in arduino
    }
  }
  else {
    setupWIFI();
  }

  Portal.handleClient();

}
